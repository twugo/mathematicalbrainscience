# coding: utf-8

# writer: Takuya Togo

import numpy as np
import learningmodel
import matplotlib.pyplot as plt

class XORSolver:
    def __init__(self):
        self._n1 = 2

    def solve(self, n2 = 2, datanum = 5000, mean=0, sigma=0.1):
        model = learningmodel.SupervisedLearningModel(self._n1, n2, mean, sigma)
        xlist, ylist = learningmodel.DataSetMaker.prepare_xor(datanum)
        error_list = model.learn(xlist, ylist)[0]

        return error_list

    def calc_correct_percentage(self, n2 = 2, datanum = 5000, mean=0, sigma=0.1, iter = 10):
        model = learningmodel.SupervisedLearningModel(self._n1, n2, mean=mean, sigma=sigma)
        xlist, ylist = learningmodel.DataSetMaker.prepare_xor(datanum)
        _, zlist = model.learn(xlist, ylist)

        percentage_list = Calculator.calc_correct_percentage(zlist, ylist, iter)

        return percentage_list

class MirrorSymmetrySolver:
    def solve(self, n1 = 6, n2 = 2, datanum = 5000, mean=0, sigma=0.1):
        model = learningmodel.SupervisedLearningModel(n1, n2, mean, sigma)
        xlist, ylist = learningmodel.DataSetMaker.prepare_mirror_symmetry(n1, datanum)
        error_list = model.learn(xlist, ylist)[0]

        return error_list

    """
    def solve_mirror_symmetry_extra(self, n1 = 6, n2 = 2, datanum = 5000):
        model = learningmodel.MirrorSymmetryLearningModel(n1, n2)

        xlist, ylist = learningmodel.DataSetMaker.prepare_mirror_symmetry(n1, datanum)
        error_list = model.learn(xlist, ylist)

        return error_list
    """

    def calc_correct_percentage(self, n1 = 6, n2 = 2, datanum = 5000, mean=0, sigma=0.1, iter = 10):
        model = learningmodel.SupervisedLearningModel(n1, n2, mean=mean, sigma=sigma)
        xlist, ylist = learningmodel.DataSetMaker.prepare_mirror_symmetry(n1, datanum)
        _, zlist = model.learn(xlist, ylist)

        percentage_list = Calculator.calc_correct_percentage(zlist, ylist, iter)

        return percentage_list

class Calculator:
    @staticmethod
    def judge_is_correct(z, y, threshold, true_value = 1, false_value = 0):
        """
        二値出力の正誤判定。出力値が閾値以上か否かでオンオフ切替し、教師信号と比較。
        Args:
            z: 出力値
            y: 教師信号
            threshold: 閾値
        """
        result = False

        if z >= threshold:
            if y == true_value:
                result = True
        else:
            if y == false_value:
                result = True
                
        return result

    @staticmethod
    def calc_is_correct(zlist, ylist, threshold=0.5, true_value = 1, false_value = 0):
        """
        正誤リストを返す。正誤は正答の時1, 誤答の時0。
        """
        return [
            1 if Calculator.judge_is_correct(zlist[i], ylist[i], threshold, true_value, false_value) else 0 
                for i in range(len(ylist))
        ]

    @staticmethod
    def calc_correct_percentage(zlist, ylist, iter=10):
        """
        出力リストと教師信号リストから、イテレータ単位で正解率を求める。
        """
        score_list = Calculator.calc_is_correct(zlist, ylist)
        score_per_iter_list = [0] * int(len(score_list)/iter)
        
        for i in range(len(score_list)):
            score_per_iter_list[int(i / iter)] += score_list[i]

        percentage_list = list(map(
            lambda x: x/iter * 0.01,
            score_per_iter_list
        ))

        return percentage_list      

class Printer:
    @staticmethod
    def plot_figure(data, title, filename = "out.png", ylabel="E"):
        fig = plt.figure()
        plt.title(title)
        plt.scatter(range(1, len(data)+1), data, s = 1, marker=".", )

        plt.xlabel("iteration")
        plt.ylabel(ylabel)
        plt.savefig(filename)

class Report:
    def q1(self, seed=0):
        np.random.seed(seed)
        # XOR
        n2 = 2
        datanum = 7000

        xor_solver = XORSolver()
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor.png")

        # ミラーシンメトリ
        n2 = 2
        datanum = 100000
        mirror_symmetry_solver = MirrorSymmetrySolver()
        error_list = mirror_symmetry_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror.png")

        # ミラーシンメトリ、n2 = 5
        n2 = 5
        error_list = mirror_symmetry_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5.png")

        return

    def q2(self, seed=0):
        np.random.seed(seed)
        n2 = 2
        datanum = 10000

        xor_solver = XORSolver()
        percentage_list = xor_solver.calc_correct_percentage(n2=n2, datanum=datanum)
        Printer.plot_figure(percentage_list, "XOR", "xor_correct_percentage.png", ylabel="correct_percentage")

        n2 = 5
        datanum = 100000
        mirror_solver = MirrorSymmetrySolver()
        percentage_list = mirror_solver.calc_correct_percentage(n2=n2, datanum=datanum)
        Printer.plot_figure(percentage_list, "Mirror Symmetry", "mirror_correct_percentage.png", ylabel="correct_percentage")

        return

    def q3(self, seed=0):
        """
        Try several different initial parameter values in learning.
        """
        np.random.seed(seed)
        xor_solver = XORSolver()
        mirror_solver = MirrorSymmetrySolver()
        '''
        # XOR sigma=0.2
        n2 = 2
        datanum = 10000
        sigma=0.2

        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig02.png")

        # XOR sigma=0.3
        sigma = 0.3
        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig03.png")

        sigma = 0.5
        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig05.png")

        sigma = 1
        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig1.png")

        sigma = 2
        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig2.png")

        sigma = 3
        error_list = xor_solver.solve(n2, datanum, sigma=sigma)
        Printer.plot_figure(error_list, "XOR", "xor_sig3.png")
        '''

        '''
        # Mirror Symmetry
        n1 = 6
        n2 = 2
        datanum = 100000

        sigma = 0.2
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig02.png")

        sigma = 0.3
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig03.png")

        sigma = 0.5
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig05.png")

        sigma = 1
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig1.png")

        sigma = 2
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig2.png")

        sigma = 3
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_sig3.png")
        '''

        # Mirror Symmetry
        n2 = 5
        datanum = 100000

        sigma = 0.2
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig02.png")

        sigma = 0.3
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig03.png")

        sigma = 0.5
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig05.png")

        sigma = 1
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig1.png")

        sigma = 2
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig2.png")

        sigma = 3
        error_list = mirror_solver.solve(n2=n2, datanum=datanum, sigma=sigma)
        Printer.plot_figure(error_list, "Mirror Symmetry", "mirror_n2_5_sig3.png")

    def q4(self, seed=0):
        """
        Try several cases of number of hidden units, n2. Interpret the trained network, 
        and explain how the network solves the XOR and Mirror Symmetry problems.
        """
        np.random.seed(seed)
        xor_solver = XORSolver()
        mirror_solver = MirrorSymmetrySolver()

        '''
        # xor
        n2 = 1
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_1.png")

        n2 = 3
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_3.png")

        n2 = 4
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_4.png")

        n2 = 5
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_5.png")

        n2 = 10
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_10.png")

        n2 = 15
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_15.png")
        
        n2 = 20
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_20.png")

        n2 = 30
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_30.png")

        n2 = 50
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_50.png")

        n2 = 75
        error_list = xor_solver.solve(n2=n2, datanum=datanum)
        Printer.plot_figure(error_list, "XOR", "xor_n2_75.png")
        '''

        '''
        # XOR
        datanum = 10000

        error_10000 = []
        for i in range(1, 101):
            n2 = i
            error_list = xor_solver.solve(n2=n2, datanum=datanum)
            error_10000.append(error_list[len(error_list)-1])
        Printer.plot_figure(error_10000, "XOR", "xor_error_10000.png", "error")
        '''

        '''
        # Mirror Symmetry
        datanum = 100000

        for i in range(1, 31):
            n2 = i
            error_list = mirror_solver.solve(n2=n2, datanum=datanum)
            filename = "mirror_n2_" + str(n2) + ".png"
            Printer.plot_figure(error_list, "Mirror", filename)
            print("file:", n2)
        '''

        datanum = 10000
        for i in range(1, 51):            
            n2 = i
            percentage_list = xor_solver.calc_correct_percentage(n2=n2, datanum=datanum)
            filename = "xor_correct_percentage_" + str(n2) + ".png"
            Printer.plot_figure(percentage_list, "XOR", filename, ylabel="correct_percentage")
            print("file:", n2)


        return

def main():
    report = Report()
    report.q4()


if __name__ == '__main__':
    main()
