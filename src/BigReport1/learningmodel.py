# coding: utf-8

# writer: Takuya Togo

import math
import numpy as np
import matplotlib.pyplot as plt
import sys

class ActivationFunctionClass:
    @staticmethod
    def sigmoid(n):
        """
        シグモイド関数
        """
        return 1 / (1 + math.e ** (-n))
    
    @classmethod
    def d_sigmoid(cls, n):
        return cls.sigmoid(n) * (1 - cls.sigmoid(n))

class LossFunctionClass:
    @staticmethod
    def least_squares(z, y):
        """
        二乗誤差
        """
        return (z-y)**2 / 2

    # @staticmethod
    # def cross_entropy(n):
    #     return


class SupervisedLearningModel:
    """
    教師あり学習モデル
    入力層、中間層1層、出力層素子１個
    """

    def __init__(self, 
                n1,
                n2,
                mean = 0,
                sigma = 0.1,
                activation_function = ActivationFunctionClass.sigmoid,
                loss_function = LossFunctionClass.least_squares,
                ):
        """
        Args: 
            n1: 入力層の素子数-1
            n2: 中間層の素子数-1
            mean: 初期の重みの平均
            sigma: 初期の重みの分散
            activation_function: 活性化関数
            loss_function:　目的関数
        """

        # 重みの作成
        self._s = np.random.normal(mean, sigma, (n2, n1+1))
        self._w = np.random.normal(mean, sigma, (1, n2+1))
        
        self._activation_function = activation_function
        self._loss_function = loss_function
        
    def calc(self, data):
        x = np.insert(data, 0, 1)
        u = np.insert(self.forward_propagation_step(x, self._s), 0, 1)
        return self.forward_propagation_step(u, self._w)

    def learn(self, xlist, ylist, mu = 0.8):
        """
        Args:
            xlist: 入力データ群
            ylist: 教師信号群
            
        Returns:
            (誤差リスト, 出力リスト)
        """
        error_list = []
        zlist = []
        for i in range(len(ylist)):
            error, z = self._learn_step(xlist[i], ylist[i], mu)
            error_list.append(error)
            zlist.append(z)
        
        return error_list, zlist

    def _learn_step(self, data, y, mu = 0.8):
        n1 = len(self._s[0]) - 1
        if(len(data) != n1):
            print('Error: 入力データの次元数とモデルの形状が一致しません。', file=sys.stderr)
            return

        x = np.insert(data, 0, 1)
        
        # 順伝搬
        self._u = np.insert(self.forward_propagation_step(x, self._s), 0, 1)
        self._z = self.forward_propagation_step(self._u, self._w)
        
        # 誤差を計算
        loss = self._loss_function(self._z, y)

        # print("z: ", self._z, "y : ", y, "loss:", loss)

        # 誤差逆伝搬
        r = (y - self._z) * self._z * (1 - self._z)
        for j in range(len(self._w[0])):
            self._w[0][j] += mu * r * self._u[j]

        for j in range(1, len(self._u)):
            r2 = r * self._w[0][j] * self._u[j] * (1 - self._u[j])
            for k in range(len(x)):
                self._s[j-1][k] += mu * r2 * x[k]
        
        return loss, self._z
        
    def forward_propagation_step(self, x, weight):
        """
        順伝搬
        Args:
            x: 前のニューロン
            weight: 重み
        """

        n1 = len(weight[0])
        if(len(x) != n1):
            print('Error: モデルの形状が正しくありません。', file=sys.stderr)
            return

        n2 = len(weight)

        outlist = np.zeros(n2)
        for i in range(n2):
            outlist[i] = self._activation_function(np.dot(x, weight[i]))
        return outlist

    def _backpropagation(self):
        return


# class MirrorSymmetryLearningModel:
#     """
#     間違えて作成した特殊なシンメトリー検出専用モデル。
#     """
#     def __init__(self, 
#                 n1,
#                 n2,
#                 activation_function = ActivationFunctionClass.sigmoid,
#                 loss_function = LossFunctionClass.least_squares,
#                 ):
#         """
#         Args: 
#             n1: 入力層の素子数
#             n2: 中間層の素子数
#             activation_function: 活性化関数
#             loss_function:　目的関数
#         """

#         # 重みの作成
#         self._s = np.random.normal(0, 0.1, (n2, n1 + 1))
#         for i in range(n2):
#             # バイアスを除いて前半を反転してマイナス、後半半分にコピー
#             self._s[i][: int((n1+1)/2) :-1] = - self._s[i][1 : int((n1 + 1)/2)+1]

#         self._w = np.random.normal(0, 0.1, (1, n2+1))
#         print(len(self._w))
#         # self._s = np.array([[1.0, 14.2, -3.6, 7.2, -7.2, 3.6, -14.2], [1.0, -14.2, 3.6, -7.2, 7.2, -3.6, 14.2]])
#         # self._w = np.array([[1.0, -8.8, -8.8]])

#         self._activation_function = activation_function
#         self._loss_function = loss_function

#     def learn(self, xlist, ylist, mu = 0.8):
#         """
#         Args:
#             xlist: 入力データ群
#             ylist: 教師信号群
#         """
#         error_list = []
#         zlist = []
#         for i in range(len(ylist)):
#             error, z = self._learn_step(xlist[i], ylist[i], mu)
#             error_list.append(error)
#             zlist.append(z)
            
#         print(self._s)
#         print(self._w)
        
#         return error_list, zlist

#     def _learn_step(self, data, y, mu = 0.8):
#         n1 = len(self._s[0]) - 1
#         if(len(data) != n1):
#             print('Error: 入力データの次元数とモデルの形状が一致しません。', file=sys.stderr)
#             return

#         x = np.insert(data, 0, 1)
#         # x = data
        
#         # 順伝搬
#         self._u = np.insert(self.forward_propagation_step(x, self._s), 0, 1)
#         self._z = self.forward_propagation_step(self._u, self._w)
        
#         # 誤差を計算
#         loss = self._loss_function(self._z, y)

#         # print("z: ", self._z, "y : ", y, "loss:", loss)

#         # 誤差逆伝搬
#         r = (y - self._z) * self._z * (1 - self._z)
#         for j in range(len(self._w[0])):
#             self._w[0][j] += mu * r * self._u[j]

#         for j in range(1, len(self._u)):
#             r2 = r * self._w[0][j] * self._u[j] * (1 - self._u[j])
#             for k in range(int(len(x)/2) + 1):
#                 self._s[j-1][k] += mu * r2 * x[k]
#             self._s[j-1][-k] = self._s[j-1][k]

#             # バイアスを除いて前半を反転してマイナス、後半半分にコピー
#             self._s[j-1][int((n1+1)/2)+1:] = - self._s[j-1][int((n1+1)/2):0:-1]

#         # print(self._s)
#         return loss, self._z

#     def forward_propagation_step(self, x, weight):
#         return SupervisedLearningModel(0, 0, self._activation_function).forward_propagation_step(x, weight)

#     def calc(self, data):
#         x = np.insert(data, 0, 1)
#         u = np.insert(self.forward_propagation_step(x, self._s), 0, 1)
#         return self.forward_propagation_step(u, self._w)

class DataSetMaker:
    @staticmethod
    def prepare_xor(datanum):
        n1 = 2
        xlist = np.random.randint(0, 2, n1 * datanum)
        xlist = np.reshape(xlist, (datanum, n1))
        ylist = [1 if x1 != x2 else 0 for x1, x2 in xlist]
        return xlist, ylist

    @staticmethod
    def prepare_mirror_symmetry(n1, datanum):
        """
        ミラーシンメトリーのデータ生成
        """
        xlist = np.random.randint(0, 2, n1 * datanum)
        xlist = np.reshape(xlist, (datanum, n1))
        ylist = [
            1 if (x[0:int(n1/2)] == x[:int((n1+1)/2)-1:-1]).all()  else 0 for x in xlist
        ]

        return xlist, ylist

def main():
    np.random.seed(0)

    n1 = 2
    n2 = 2
    model = SupervisedLearningModel(n1, n2)
    
    datanum = 10000
    xlist, ylist = DataSetMaker.prepare_xor(datanum)
    error_list = model.learn(xlist, ylist)

if __name__ == '__main__':
    main()
